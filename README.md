Football data engineering exercise
You are provided with three sources of data:
1. Users - which resides in a transactional MongoDB collection football_assignment.users
2. Clubs - which resides in a transactional MongoDB collection football_assignment.clubs
3. Clubs Tweets - which resides in our S3-based data lake in parquet format, in the
following link - s3://aui-lab-data-engineer-resources/tweets/clubs-tweets.parquet
Part 1: API
Build a RESTful API with an endpoint that receives a list of user_ids and returns an array of
objects (an object per user-id) with the user_id, name, club, club country, club total wins in
UEFA championships (Total). Your endpoint should support pagination (default size of 10).
Part 2: ETL
Build an ETL process that pulls data from the tweets S3 source, enriches it using the API from
part 1, and loads enriched data into hive tables in your machine. Please consider the following:
1. Output file format should be parquet.
2. The table should contain a column called hashtags, containing all the hashtags extracted
from the tweet_entities column, formatted by a comma-separated string.
3. Tweets in the hive table should be stored according to their country and club.
Part 3: Analysis
Using the resulting data from the ETL process in part 2, build a RESTful API with an endpoint
per each of the questions below. For each query, add a docstring that elaborates how the query
is optimized to answer the analytical question. Also, aim to do as much work on the query level
rather than in subsequent Python code (as done in a real production environment):
1. Which of Barcelona or Real Madrid, on the club level, has the largest number of tweets
among all of their accounts?
2. Which club, of the top three clubs in terms of total championships-wins, has the highest
reactions in their Twitter accounts in 2019? (reactions is defined as tweet_favorite_count
+ tweet_retweeted)
3. What are, on the month level, the top two trending tags between 2018 and 2019,
inclusive? (trending is the number of occurrences for this tag in the tweets)
Part 4: Design 
4. Implement an architectural diagram that effectively illustrates the components of the
above solution
5. Assuming the clubs-tweets data is being ingested in real-time into a data stream (instead
of being available in the S3-based data lake), implement a second diagram for a solution
that gets the top five trending hashtags from the players in the past 14 days, and also
stores the raw data into S3 for future analysis


