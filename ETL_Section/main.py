import os
from datetime import timedelta

import pandas as pd

import json
from types import SimpleNamespace

from prefect import Flow, task
from prefect.agent.local import LocalAgent
from prefect.executors import LocalDaskExecutor, LocalExecutor
from prefect.run_configs import LocalRun
from prefect.schedules import IntervalSchedule

schedule = IntervalSchedule(interval=timedelta(minutes=1))


def extract_data_from_s3():
    return 0


def clean_data():
    return 0


def load_data():
    return 0


with Flow("Football_assignment", schedule, executor=LocalExecutor(), ) as flow:
    extract_data_from_s3()
    clean_data()
    load_data()

flow.register(project_name='AUI_Task')
LocalAgent().start()
