import motor.motor_asyncio

MONGO_DETAILS = "mongodb+srv://de-assignment:ZxULbW7O3wPs0Gpi@aui-de-assignments.cohiy.mongodb.net/admin"

client = motor.motor_asyncio.AsyncIOMotorClient(MONGO_DETAILS)

database = client.football_assignment

user_collection = database["users"]


class UserClub:

    def __init__(self, user_id, name, club, club_country):
        self.user_id = user_id
        self.name = name
        self.club = club
        self.club_country = club_country


# def user_helper(user) -> dict:
#     return {
#         "external_id": user["external_id"],
#         "name": user["name"],
#         "Club": user["club"],
#     }
#
#
# def club_helper(club) -> dict:
#     return {
#         "Club": club["Club"],
#         "Country": club["Country"],
#         "Total": club["Total"]
#     }


def user_club_helper(club_user) -> dict:
    return {
        "external_id": club_user["external_id"],
        "name": club_user["name"],
        "Club": club_user["Club"],
        "Country": club_user["Country"],
        "Total": club_user["Total"]
    }


import pandas as pd


def get_database():
    CONNECTION_STRING = "mongodb+srv://de-assignment:ZxULbW7O3wPs0Gpi@aui-de-assignments.cohiy.mongodb.net/admin"

    # Create a connection using MongoClient. You can import MongoClient or use pymongo.MongoClient
    from pymongo import MongoClient
    client = MongoClient(CONNECTION_STRING)

    # Create the database for our example (we will use the same database throughout the tutorial
    # print(client['football_assignment'])
    return client['football_assignment']


# # Retrieve all users present in the database
def retrieve_users():
    dbname = get_database()
    users = dbname.users.find({}, {'external_id': 1, 'club': 1})
    users_df = pd.DataFrame(users)
    return users_df['external_id'].to_list()


# #

# def retrieve_clubs():
#     dbname = get_database()
#     clubs = dbname.clubs.find({}, {'Club': 1, 'Country': 1, 'Total': 1})
#     return pd.DataFrame(clubs)


from bson.objectid import ObjectId


def replace_v(value):
    value = value.replace("'", '"')
    return value


async def create_club_user(id_list: str) -> list:
    id_list = id_list.replace('"', '')
    id_list = id_list.replace("'", '')
    id_list = id_list.replace(' ', '')
    id_list = id_list.replace('[', '')
    id_list = id_list.replace(']', '')
    id_list = id_list.replace(']', '')
    id_list = list(id_list.split(","))
    id_list = [int(i) for i in id_list]

    dbname = get_database()
    users_collection = dbname['users']
    clubs_collection = dbname['clubs']
    users = users_collection.find({"external_id": {
        "$in": id_list}}, {"club": 1, "external_id": 1,"name": 1})
    users_df = pd.DataFrame(users)
    # clubs_list =(users_df.club.map(replace_v)).to_list()

    clubs = clubs_collection.find({"Club": {"$in": users_df['club'].to_list()}}, {"Club": 1, "Country": 1, "Total": 1})
    clubs_df = pd.DataFrame(clubs)
    users_club_df = pd.merge(users_df, clubs_df, how='inner', left_on='club', right_on='Club')
    # with open('readme.txt', 'w') as f:
    #     for index, row in users_club_df.iterrows():
    #         f.write(str(row))
    user_club_list = [(UserClub(row.external_id, row.name, row.club, row.Country)) for index, row in
                      users_club_df.iterrows()]

    return user_club_list

# async def retrieve_clubs(user_ids):
