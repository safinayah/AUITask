from fastapi import FastAPI
# from server.routes.user import  router as UserRouter
# from server.routes.club import router as ClubRouter
from server.routes.user_club import router as UserClubRouter


app = FastAPI()
app.include_router(UserClubRouter, tags=["User_Club"], prefix="/user_club")
