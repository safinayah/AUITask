from typing import Optional

from pydantic import BaseModel, EmailStr, Field
from fastapi_pagination import Page


class UserSchema(BaseModel):
    external_id: str = Field(...)
    name: str = Field(...)
    club: str = Field(...)

    class Config:
        schema_extra = {
            "example": {
                'external_id': 904718268,
                'name': 'LFC USA',
                'club': 'Liverpool'
            }
        }


def ResponseModel(data, message):
    return {
        "data": [data],
        "code": 200,
        "message": message,
    }


def ErrorResponseModel(error, code, message):
    return {"error": error, "code": code, "message": message}
