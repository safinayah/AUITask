from fastapi import APIRouter
from server.database import create_club_user
from server.models.user import (
    ResponseModel
)

router = APIRouter()


@router.get("/id/")
async def get_user_club_data(id_list: str) -> dict:
    users = await create_club_user(id_list)
    if users:
        return ResponseModel(users, "Data retrieved successfully")
    return ResponseModel(users, "Empty list returned")
